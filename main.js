function getXY(canvas, event) {
    const rect = canvas.getBoundingClientRect();
    const x = event.clientX - rect.left;
    const y = event.clientY - rect.top;
    return {x, y};
}

const canvas_draw = document.getElementById('draw_space');
canvas_draw.onmousedown = function(event0) {
const ctx = canvas_draw.getContext("2d");
let point0 = getXY(canvas_draw, event0);

canvas_draw.onmousemove = function(event1) {
    const point1 = getXY(canvas_draw, event1);
    ctx.beginPath();
    ctx.lineCap = "round";
    ctx.moveTo(point0.x, point0.y);
    ctx.lineTo(point1.x, point1.y);
    ctx.fill();
    ctx.stroke();
    // mise a jour de la position
    point0 = point1;
    };
};

//lorsqu'on leve la souris
canvas_draw.onmouseup = function(evt) {
    canvas_draw.onmousemove = {};
};


const button_clear = document.getElementById('button_clear')
button_clear.onclick = function() {
    const width = canvas_draw.clientWidth;
    const height = canvas_draw.clientHeight;
    const ctx = canvas_draw.getContext("2d");
    ctx.beginPath();
    ctx.clearRect(0, 0, width, height);
    ctx.stroke();
}